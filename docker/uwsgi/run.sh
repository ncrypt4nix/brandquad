#!/bin/bash
python /srv/project/manage.py migrate --noinput &
python /srv/project/manage.py collectstatic --noinput &
uwsgi /srv/project/docker/uwsgi/uwsgi.ini
