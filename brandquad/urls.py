from django.contrib import admin
from django.urls import path, include
from django_filters.views import FilterView
from test_app.filters import AccessLogFilter


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(('test_app.urls', 'api'), namespace='api')),
    path(
        '',
        FilterView.as_view(
            template_name='test_app/index.html',
            filterset_class=AccessLogFilter
        )
    ),
]
