from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


class ResultAndMaxPagePaginator(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'

    def get_paginated_response(self, data):
        return Response({
            'num_pages': self.page.paginator.num_pages,
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'results': data,
        })
