import django_filters
from test_app.models import AccessLog


class AccessLogFilter(django_filters.FilterSet):
    class Meta:
        model = AccessLog
        fields = (
            'ip',
            'date',
            'method',
            'uri',
            'code',
        )
