from collections import namedtuple
from django.db import models


class AccessLog(models.Model):
    METHODS_VARIANT = {
            'UNDEFINED': 0,
            'GET': 1,
            'HEAD': 2,
            'POST': 3,
            'PUT': 4,
            'DELETE': 5,
            'CONNECT': 6,
            'OPTIONS': 7,
            'TRACE': 8,
            'PATCH': 9,
            'SEARCH': 10,
    }
    METHODS = namedtuple(
        'METHODS',
        list(METHODS_VARIANT.keys())
    )(**METHODS_VARIANT)
    ip = models.GenericIPAddressField()
    date = models.DateTimeField()
    method = models.PositiveSmallIntegerField(
        choices=tuple(
            (value, key)
            for key, value in METHODS._asdict().items()
        )
    )
    uri = models.URLField()
    protocol = models.CharField(max_length=16)
    code = models.PositiveSmallIntegerField()
    length = models.PositiveIntegerField()
    url = models.URLField()
    browser = models.CharField(max_length=256)

    class Meta:
        indexes = [models.Index(fields=['date'])]
