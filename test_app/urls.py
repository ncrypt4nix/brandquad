from django.urls import path
from rest_framework import routers
from test_app.views import AccessLogViewSet, AccessLogXLSXViewSet


router = routers.SimpleRouter()
router.register(r'access_log', AccessLogViewSet)
router.register(r'access_log_xslx', AccessLogXLSXViewSet, basename='accesslogxlsx')
urlpatterns = router.urls
