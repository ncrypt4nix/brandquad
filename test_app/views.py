from django.views.generic.list import ListView
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from drf_renderer_xlsx.mixins import XLSXFileMixin
from drf_renderer_xlsx.renderers import XLSXRenderer
from test_app.models import AccessLog
from test_app.serializers import AccessLogSerializer
from test_app.filters import AccessLogFilter


class AccessLogViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = AccessLog.objects.all().order_by('date')
    serializer_class = AccessLogSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = (
        'ip',
        'date',
        'method',
        'uri',
        'code',
    )
    ordering_fields = ('date',)


class AccessLogXLSXViewSet(XLSXFileMixin, AccessLogViewSet):
    renderer_classes = (XLSXRenderer,)
    filename = 'access_log.xlsx'
