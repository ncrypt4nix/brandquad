import os
import time
import re
import requests
import tempfile
import datetime
import threading
from queue import Queue
from collections import namedtuple
from progress.bar import Bar
from progress.spinner import Spinner
from django.core.management.base import BaseCommand, CommandError
from django.db import DataError
from test_app.models import AccessLog


class Command(BaseCommand):
    help = "download access log file and parse it"
    DOWNLOAD_LINK = 'http://www.almhuette-raith.at/apache-log/access.log'
    REGEX_GROUPS = namedtuple(
        'REGEX_GROUPS',
        [
            'ip',
            'date',
            'method',
            'uri',
            'protocol',
            'code',
            'length',
            'url',
            'browser'
        ]
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        regex_group = self.REGEX_GROUPS(
            ip=r'(?P<ip>(?:\d{1,3}\.){3}\d{1,3})',
            date=r'(?P<date>\[.+\])',
            method=r'(?P<method>\w+)',
            uri=r'(?P<uri>\S+)',
            protocol=r'(?P<protocol>\S+)',
            code=r'(?P<code>\d+)',
            length=r'(?P<length>\d+)',
            url=r'(?P<url>\S+)',
            browser=r'(?P<browser>[^"]+)',
        )
        self.regex = re.compile(
            f'{regex_group.ip} - - {regex_group.date} '
            f'"{regex_group.method} {regex_group.uri} {regex_group.protocol}"'
            f' {regex_group.code} {regex_group.length} '
            f'"{regex_group.url}" '
            f'"{regex_group.browser}" "-"'
        )
        self.slice_fields = {
            i.name: i.max_length
            for i in AccessLog._meta.fields if getattr(i, 'max_length')
        }

    def handle(self, *args, **options):
        BLOCK_SIZE = 4096
        LOG_BLOCK_COUNT = 500
        QUEUE_LOGS_LIMITER = 50
        response = requests.get(self.DOWNLOAD_LINK, stream=True)
        content_length = int(response.headers.get('content-length'))
        blocks_len = content_length // BLOCK_SIZE
        fd, fd_path = tempfile.mkstemp()
        # download file
        with Bar('Download', max=blocks_len, suffix='%(percent)d%%') as bar:
            with open(fd_path, "wb") as file:
                for i in bar.iter(response.iter_content(chunk_size=BLOCK_SIZE)):
                    file.write(i)
            bar.finish()

        queue_logs = Queue()

        def save_to_db():
            """ Closure queue:queue_logs  and bulk create logs in db """
            while True:
                logs = queue_logs.get()
                if logs is None:
                    return
                try:
                    AccessLog.objects.bulk_create(logs)
                except (ValueError, DataError):
                    raise CommandError('Error with parse and save logs')

        # parse file
        with os.fdopen(fd, "r") as file:
            last_record = None
            try:
                last_record = AccessLog.objects.latest('date').date
            except AccessLog.DoesNotExist:
                pass
            line_count = sum(1 for line in file)
            file.seek(0, 0)
            with Bar('Parsing', max=line_count, suffix='%(percent)d%%') as bar:
                thread = threading.Thread(target=save_to_db)
                thread.start()
                block_logs = []
                counter = 0
                for line in bar.iter(file):
                    result = re.match(self.regex, line)
                    if not result:
                        continue
                    group = self.REGEX_GROUPS(
                        ip=result.group('ip')[:self.slice_fields['ip']],
                        date=datetime.datetime.strptime(
                            result.group('date'),
                            '[%d/%b/%Y:%H:%M:%S %z]'
                        ),
                        method=AccessLog.METHODS_VARIANT.get(
                            result.group('method').upper(),
                            AccessLog.METHODS.UNDEFINED
                        ),
                        uri=result.group('uri')[:self.slice_fields['uri']],
                        protocol=result.group('protocol')[
                            :self.slice_fields['protocol']
                        ],
                        code=int(result.group('code')),
                        length=int(result.group('length')),
                        url=result.group('url')[:self.slice_fields['url']],
                        browser=result.group('browser')[
                            :self.slice_fields['browser']
                        ],
                    )
                    if last_record is not None and group.date < last_record:
                        continue
                    block_logs.append(AccessLog(**group._asdict()))
                    if LOG_BLOCK_COUNT <= counter:
                        # block RAM overflow
                        while QUEUE_LOGS_LIMITER < queue_logs.qsize():
                            time.sleep(1)
                        counter = 0
                        queue_logs.put(block_logs)
                        block_logs = []
                    counter += 1
                queue_logs.put(block_logs)

                # stop threading
                def wait_end():
                    """Closure queue:queue_logs and show spinner"""
                    spinner = Spinner('Completion of work... ')
                    while not queue_logs.empty():
                        spinner.next()

                spinner_thread = threading.Thread(target=wait_end)
                spinner_thread.start()
                queue_logs.put(None)
                thread.join()
                spinner_thread.join()
                bar.finish()
        self.stdout.write(self.style.SUCCESS('Done. '))
