document.addEventListener("DOMContentLoaded", (event) => {
    var app = new Vue({
        el: '#app',
        data: {
            logs: [],
            pageRange: [],
            page: 1,
            querystring: '',
            responcesLength: 0,
            uniqueIPCount: 0,
            uniqueMethodsLength: [],
            IPTopTen: [],
            loading: true,
        },
        methods: {
            buildURL: function(mainURL) {
                let url = `${mainURL}?page=${this.page}`
                if (this.querystring) {
                    url += '&' + this.querystring
                }
                return url
            },
            update: async function() {
                this.loading = true
                let url = this.buildURL(API_URL)
                let response = await fetch(url)
                let json = await response.json()
                this.logs = json.results
                this.pageRange = this.pagination(this.page, json.num_pages)
                this.responcesLength = this.logs.reduce((accumulator, x) => accumulator + x.length, 0)
                this.uniqueIPCount = this.logs.filter(log => log.ip_count == 1).length
                this.IPTopTen = [
                    ...new Map(this.logs.map(x => [x.ip, x])).values()
                ].sort(
                    (a, b) => {return a.ip_count < b.ip_count}
                ).slice(0, 10)
                this.uniqueMethodsLength = [
                    ...new Map(this.logs.map(x => [x.method, x])).values()
                ].sort(
                    (a, b) => {return a.method_count < b.method_count}
                )
                this.loading = false
            },
            pagination: (page, max_page) => {
                const delta = 5
                let pageRange = [page]
                for (let i = 1; i < delta; i++) {
                    if (0 < page - i) {
                        pageRange.unshift(page - i)
                    }
                    if (page + i < max_page) {
                        pageRange.push(page + i)
                    }
                }
                if (1 <= page - delta) {
                    pageRange.unshift(1)
                }
                if (page + delta < max_page) {
                    pageRange.push(max_page)
                }
                return pageRange
            },
            formFilterSend: function(event) {
                event.preventDefault()
                this.page = 1
                this.querystring = new URLSearchParams(new FormData(event.target)).toString()
                this.update()
            },
            downloadXLSX: async function() {
                window.open(this.buildURL(API_XLSX_URL), "_blank");
            },
        },
        created: function() {
            this.update()
        },
        delimiters: ['[[', ']]'],
    })
})
