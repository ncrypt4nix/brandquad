import pandas as pd
from rest_framework import serializers
from test_app.models import AccessLog


class AccessLogSerializer(serializers.ModelSerializer):
    method = serializers.CharField(source='get_method_display')
    ip_count = serializers.SerializerMethodField()
    method_count = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        df = pd.DataFrame(
            [(i.ip, i.method) for i in self.instance],
            columns=['ip', 'method']
        )
        self.ip_unique = df.groupby('ip').count()
        self.method_unique = df.groupby('method').count()

    class Meta:
        model = AccessLog
        fields = (
            'ip',
            'date',
            'method',
            'uri',
            'protocol',
            'code',
            'length',
            'url',
            'browser',
            'ip_count',
            'method_count',
        )

    def get_ip_count(self, obj):
        return self.ip_unique.loc[obj.ip][0]

    def get_method_count(self, obj):
        return self.method_unique.loc[obj.method][0]
