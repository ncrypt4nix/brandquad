FROM python:3.7

ENV PYTHONUNBUFFERED 1
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install --no-install-recommends python3-dev build-essential && apt-get clean
RUN mkdir -p /srv/project/ && mkdir -p /srv/project/media && mkdir -p /srv/project/static
WORKDIR /srv/project/
RUN pip install --upgrade pip
COPY requirements.txt /srv/project/
RUN pip install -r requirements.txt
COPY . /srv/project/
ARG DJANGO_SETTINGS_MODULE=brandquad.settings
ENV DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
